# KWS Homework

The experiments and their results can be found in [experiments_and_visualizations.ipynb](./experiments_and_visualizations.ipynb). All required materials are downloaded in the Jupyter Notebook.

If you don't need the notebook then use
```shell script
pip install -qr requirements.txt
```

## Base model

Base model reaches quality of `~2e-5`. Checkpoint can be loaded with the following command
```shell script
gdown --id 1RD44iRblWA4GY7wglCLomtT2j8_e7hTl -O base_model.pth
```
Hyperparameters for base model are defined in [configs/full_model_params.py](./configs/full_model_params.py).

## Streaming

Code for streaming is found in [src/streaming.py](./src/streaming.py). I tried streaming with multiple window lengths and multiple streaming steps. Test audio is made from loud crowd noises and a keyword sample. You can listen to this audio in the Jupyter notebook with experiments. Keyword probabilities can be seen below.

![streaming results](imgs/streaming.png)

Red dashed lines mark the borders of the keyword in test audio. I also added smoothing with rolling window of size 5. It gets rid of random jumps, at least in plots with good settings.

You can see that window size and step size are very important for getting streaming with good quality. Small windows and small step size lead to extremely noisy probabilities. With large streaming step there is a risk of missing a crucial part of audio.

## Speedup and Compression

I tried 3 different setups:
1) Dark knowledge distillation
2) Dark knowledge distillation with attention distillation
3) Dark knowledge distillation with qint8 quantization

The results can be seen in the following figure
![speedup results](imgs/comparison.png)

In relation to the base model, the results are as follows

Model                          | Memory compression rate   | Speed compression rate   
------------------------------ | ------------------------: | ------------------------:
dark knowledge                 |                   10.5469 |                    6.1861
dark knowledge + attn          |                   14.8332 |                   10.1558
dark knowledge + quantization  |                   10.8696 |                    6.1861

### Scoring

1) Memory scoring is based on `model.state_dict()` size after saving it to the hard drive. The function for memory scoring can be found in [./src/profiling.py](./src/profiling.py).
2) Speed scoring is based on FLOPs value that is provided by [thop](https://github.com/Lyken17/pytorch-OpCounter) package for Python3. For quantized models FLOPs value is the same as for the original model.

### Dark knowledge distillation

Hyperparameters for this model can be found in [./configs/dark_knowledge_params.py](./configs/dark_knowledge_params.py). It was trained with `DarkKnowledgeLoss` from [./src/distillation.py](./src/distillation.py). Training logs can be found in [./experiments_and_visualizations.ipynb](./experiments_and_visualizations.ipynb). The quality of the final model is `3.2e-5`.

`DarkKnowledgeLoss` is defined by the following formula, where $T$ is probability temperature:
```math
\mathcal{L} = \alpha \cdot CrossEntropy({\rm student\_probs(T)},\ {\rm teacher\_probs(T)}) + (1 - \alpha) \cdot CrossEntropy({\rm student\_probs(1.)},\ {\rm true\_probs})
```
Dark knowledge distillation was proposed in [1]. In the paper the first summand is also multiplied by $`T^2`$. In my experiments such loss gave worse results.

The best model was trained with $`\alpha=0.6`$ and $`T=0.8`$.

I noted that loss's parameters are very sensitive to the teacher model's quality. A model with slightly better quality than my baseline may train better with high (> 1) temperature but for the baseline I obtained much better results with low temperature.

### Dark knowledge with quantization

This setup uses trained model from previous experiment. Quantization is dynamic and affects GRU and linear layers. Curiously enough, quantization of only linear layers slightly increased memory consumption. Quantization of both linear layers and GRU layers gives a slight edge both in quality and memory consumption as can be seen in the figures above.

The quality of the final model is `3.0e-5`.

### Dark knowledge with attention distillation

Hyperparameters for this model can be found in [./configs/dark_knowledge_attn_params.py](./configs/dark_knowledge_attn_params.py). It was trained with `CombinationLoss` from [./src/distillation.py](./src/distillation.py), which basically is a linear combination of `AttentionLoss` and `DarkKnowledgeLoss` from the same module. Training logs can be found in [./experiments_and_visualizations.ipynb](./experiments_and_visualizations.ipynb). The quality of the final model is `5e-5`.

`AttentionLoss` minimizes cross-entropy between student's attention scores and teacher's attention scores.

Attention distillation significantly improves training process. I was able to further decrease the number of kernels in the convolution layers. This led to slight dip in quality but FLOPs value noticibly drop. This is the best model out of all my experiments.

## References
1) Hinton, Geoffrey, Oriol Vinyals, and Jeff Dean. "Distilling the knowledge in a neural network." arXiv preprint arXiv:1503.02531 (2015).



