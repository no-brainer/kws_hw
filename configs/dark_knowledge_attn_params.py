import dataclasses

from configs.full_model_params import BaseModelConfig


@dataclasses.dataclass
class DarkKnowledgeAttnConfig(BaseModelConfig):
    num_epochs: int = 25
    gru_num_layers: int = 2
    hidden_size: int = 16
    cnn_out_channels: int = 2
    weight_decay: float = 0.0
    learning_rate: float = 5e-4
