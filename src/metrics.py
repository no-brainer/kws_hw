import numpy as np
import torch


# FA - true: 0, model: 1
# FR - true: 1, model: 0
def count_fa_fr(preds, labels):
    fa = torch.sum(preds[labels == 0])
    fr = torch.sum(labels[preds == 0])

    # torch.numel - returns total number of elements in tensor
    return fa.item() / torch.numel(preds), fr.item() / torch.numel(preds)


def get_au_fa_fr(probs, labels):
    sorted_probs, _ = torch.sort(probs)
    sorted_probs = torch.cat((torch.Tensor([0]), sorted_probs, torch.Tensor([1])))
    labels = torch.cat(labels, dim=0)

    fas, frs = [], []
    for prob in sorted_probs:
        preds = (probs >= prob) * 1
        fa, fr = count_fa_fr(preds, labels)
        fas.append(fa)
        frs.append(fr)

    # ~ area under curve using trapezoidal rule
    return -np.trapz(frs, x=fas)
