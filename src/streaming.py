import torch
import torch.nn.functional as F


class StreamingKWS:

    def __init__(self, model, max_window_length, streaming_step_size=1):
        self.model = model
        self.max_window_length = max_window_length
        self.streaming_step_size = streaming_step_size

    def __call__(self, input):
        self.model.eval()
        probs = []
        with torch.no_grad():
            curr_hidden_state = None
            for start_idx in range(0, input.size(-1) - self.max_window_length + 1, self.streaming_step_size):
                output, curr_hidden_state = self.model(
                    input[..., start_idx:start_idx + self.max_window_length],
                    curr_hidden_state,
                    return_hidden=True
                )
                output = F.softmax(output, dim=-1)
                probs.append(output[..., 1].unsqueeze(-1))

        probs = torch.cat(probs, dim=-1)
        return probs
