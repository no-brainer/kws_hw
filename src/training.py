from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F

from src.metrics import get_au_fa_fr


def train_epoch(model, opt, loader, log_melspec, device):
    model.train()
    acc = None
    for i, (batch, labels) in tqdm(enumerate(loader), total=len(loader)):
        batch, labels = batch.to(device), labels.to(device)
        batch = log_melspec(batch)

        opt.zero_grad()

        # run model # with autocast():
        logits = model(batch)
        # we need probabilities so we use softmax & CE separately
        probs = F.softmax(logits, dim=-1)
        loss = F.cross_entropy(logits, labels)

        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 5.)

        opt.step()

        # logging
        argmax_probs = torch.argmax(probs, dim=-1)
        acc = torch.sum(argmax_probs == labels) / torch.numel(argmax_probs)

    return acc


@torch.no_grad()
def validation(model, loader, log_melspec, device):
    model.eval()
    all_probs, all_labels = [], []
    for i, (batch, labels) in tqdm(enumerate(loader)):
        batch, labels = batch.to(device), labels.to(device)
        batch = log_melspec(batch)

        output = model(batch)

        probs = F.softmax(output, dim=-1)
        all_probs.append(probs[:, 1].detach().cpu())
        all_labels.append(labels.cpu())

    au_fa_fr = get_au_fa_fr(torch.cat(all_probs, dim=0).cpu(), all_labels)
    return au_fa_fr
