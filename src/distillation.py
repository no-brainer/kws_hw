from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F


class DarkKnowledgeLoss(nn.Module):

    def __init__(self, alpha, temperature):
        super().__init__()
        self.alpha = alpha
        self.t = temperature

    def forward(self, student_logits, teacher_logits, true_labels, *args, **kwargs):
        teacher_part = F.kl_div(
            F.log_softmax(student_logits / self.t, dim=-1),
            teacher_logits / self.t,
            reduction="batchmean"
        )
        true_part = F.cross_entropy(student_logits, true_labels)
        return self.alpha * teacher_part + (1 - self.alpha) * true_part


class AttentionLoss(nn.Module):

    def __init__(self):
        super().__init__()

    def forward(self, student_scores, teacher_scores, *args, **kwargs):
        return F.kl_div(
            torch.log(torch.clamp(student_scores, 1e-12, 1 - 1e-12)),
            teacher_scores,
            reduction="batchmean"
        )


class CombinationLoss(nn.Module):

    def __init__(self, attention_criterion, dark_knowledge_criterion, attention_weight):
        super().__init__()
        self.attention_weight = attention_weight
        self.attention_criterion = attention_criterion
        self.dark_knowledge_criterion = dark_knowledge_criterion

    def forward(self, *args, **kwargs):
        return (self.dark_knowledge_criterion(*args, **kwargs)
                + self.attention_weight * self.attention_criterion(*args, **kwargs))


def distill_train(teacher_model, student_model, criterion, optimizer, dataloader, log_melspec, device):
    student_model.train()
    teacher_model.eval()
    acc = None
    for i, (batch, labels) in tqdm(enumerate(dataloader), total=len(dataloader)):
        batch, labels = batch.to(device), labels.to(device)
        batch = log_melspec(batch)

        optimizer.zero_grad()

        with torch.no_grad():
            teacher_logits, attention_scores = teacher_model(batch, return_attention=True)
            results = {
                "teacher_logits": teacher_logits,
                "teacher_scores": attention_scores,
                "true_labels": labels,
            }

        student_logits, attention_scores = student_model(batch, return_attention=True)
        results["student_logits"] = student_logits
        results["student_scores"] = attention_scores

        loss = criterion(**results)

        loss.backward()
        nn.utils.clip_grad_norm_(student_model.parameters(), 5.)

        optimizer.step()

        preds = torch.argmax(student_logits, dim=-1).detach()
        acc = torch.sum(preds == labels) / torch.numel(preds)

    return acc
